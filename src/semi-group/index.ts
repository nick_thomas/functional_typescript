import {ordNumber} from "fp-ts/lib/Ord"
import {getJoinSemigroup, getMeetSemigroup} from "fp-ts/lib/Semigroup"
import {struct} from "fp-ts/Semigroup"

interface Semigroup<A>{
	concat: (x: A, y: A) => A
}

const semigroupProduct: Semigroup<number> = {
	concat: (x,y) => x * y
}

const semigroupSum : Semigroup<number> = {
	concat: (x,y) => x + y
}

const semigroupString: Semigroup<string> = {
	concat: (x,y) => x + y
}

function getFirstSemigroup<A = never>() : Semigroup<A>{
	return { concat: (x,y) => x}
}

function getLastSemigroup<A = never>(): Semigroup<A>{
	return {concat: (x,y)=> y}
}

function getArraySemigroup<A = never>(): Semigroup<Array<A>>{
	return {concat: (x,y) => x.concat(y)}
}

function of <A>(a: A): Array<A>{
	return [a]
}

const semigroupMin: Semigroup<number> = getMeetSemigroup(ordNumber)

const semigroupMax: Semigroup<number> = getJoinSemigroup(ordNumber)

type Point = {
	x: number
	y: number
}

const semigroupPoint: Semigroup<Point> = {
	concat: (p1: Point,p2 : Point) => ({
		x: semigroupSum.concat(p1.x,p2.x),
		y: semigroupSum.concat(p1.y,p2.y)
	})
}


export const main = () => {
	console.log(semigroupMin.concat(2,1));
	console.log(semigroupMax.concat(2,1));
}
