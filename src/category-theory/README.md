### Category theory illustrated notes

# Todo -> The Power of Composition

`https://boris-marinov.github.io/category-theory-illustrated/02_category/`

**What is a function?**

It is a relationship between two sets that matches each element of the set.

---

**What are various mappings that can be expressed with functions?**

- many elements from source(domain) point to same element in target(codomain)
- some elements from codomain(target) are not involved.

---

**What is some rules about the relationships which cannot be valid?**

- Domain element which is not mapped to anything in codomain
- Domain element which is mapped to more than one element in codomain

---

**What is identity function?**

It is a function which mapes every element in a set to itself.

---

**How are sets used in programming?**

Types/Classes are sets.

---

**What is functional composition?**

```mermaid
graph TD
    A[object1] -->|f| B(object2)
    B --> |h| C(object3)
    A --> |g| C
```

---

**What is Isomorphisms?**

A function which is invertible i.e -> if you flip the function you get another function.

---

**What is the difference between Isomorphism and Equality in Category theory?**

In category theory, the concept of isomorphism is strongly related to the concept of equality. ≠

---
