// https://dev.to/gcanti/getting-started-with-fp-ts-setoid-39f3
import {contramap, getStructEq} from "fp-ts/lib/Eq"

interface Eq<A> {
	readonly equals: (x:A, y: A) => boolean
}

const eqNumber: Eq<number> = {
	equals: (x,y) => x === y
}

const eqString: Eq<string> = {
	equals: (x,y) => x === y
}

function elem<A>(E: Eq<A>) : (a: A, as : Array<A>) => boolean {
	return (a,as) => as.some(item => E.equals(item,a))
}

type Point = {
	x: number,
	y:number
}

const eqPoint : Eq<Point> = {
	equals: (p1,p2) => p1.x === p2.x && p1.y === p2.y
}

const eqPoint2:Eq<Point> = getStructEq({
	x:eqNumber,
	y: eqNumber
})

type User = {
	userId: number,
	name:string
}

const eqUser = contramap((user:User)=>user.name)(eqString)


export const main = () => {
	let value = elem(eqNumber)(1,[1,2,3])
	let secondValue = elem(eqNumber)(1,[2,3,4])
	let newPoint:Point = {
		x: 1,
		y: 2
	} as Point
	let newPoint2:Point = {
		x: 1,
		y: 2
	} as Point
	let pointValue = eqPoint2.equals(newPoint,newPoint2)
	let eqUserValue = eqUser.equals({userId:1,name:"Nick"},{userId:1,name:"Tom"})
	console.log(eqUserValue)
}
