**What are Objects?**

Objects is a collection of objects, which are black boxes that can't be inspected.

---

**What are Morhphisms?**

Arrows/Connection between the objects.

---

**When is Composition property valid?**

When the following properties are valid

- composition of morphisms
- associativity
- identity

---

**What is composition of morphisms?**

If there is a morphism between A -> B, and B -> C, there should be a third morphism for A -> C

---

**What are category?**

- objects are types
- morphisms are functions
- (.) operator is usual function composition

---
