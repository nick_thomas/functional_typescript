import {Eq} from "fp-ts/lib/Eq"
import {fromCompare, getDualOrd} from "fp-ts/lib/Ord"

type Ordering = -1 | 0 | 1

interface Ord<A> extends Eq<A>{
	readonly compare: (x: A, y: A) => Ordering
}

const ordNumber: Ord<number> = fromCompare((x,y)=> (x < y ? -1 : x > y ? 1 : 0))

function min<A>(O: Ord<A>) : (x: A, y: A) => A {
	return (x,y) => (O.compare(x,y) === 1 ? y : x)
}

function max<A>(O: Ord<A>): (x: A, y: A)=> A {
	return min(getDualOrd(O))
}

type User = {
	name: string,
	age: number,
}

const byAge:Ord<User> = fromCompare((x,y) => ordNumber.compare(x.age,y.age))

export const main = () => {
	const user1: User ={
		name: "nick",
	age: 25,
	};
	const user2: User = {
		name: "steve",
		age: 35
	}
	const getYounger = min(byAge)
	let output = getYounger(user2,user1)
	const getOlder = max(byAge)
	output = getOlder(user2,user1)
	console.log(output)
}
