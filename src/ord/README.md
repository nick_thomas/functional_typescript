*What is type class Ord?*

It is used to contain types that admit a total ordering.

---

*What are laws for Ord?*

1. Reflexivity :-> compare(x,y) === 0 for all x in A
2. Antisymmetry:-> if compare(x,y) <=0 and compare(y,x) <= 0 then x is equals to y for all x,y in A
3. Transitivity:-> if compare(x,y) <= 0 and compare(y,z) <= 0 then compare (x,z) <= 0 for all x,y, z in A
4. Must comply with Eq's equal

---
