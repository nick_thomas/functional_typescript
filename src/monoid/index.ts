import { fold, getStructMonoid, Monoid } from "fp-ts/lib/Monoid";
import { getApplyMonoid, getFirstMonoid, none, some } from "fp-ts/lib/Option";

const monoidSum: Monoid<number> = {
  concat: (x, y) => x + y,
  empty: 0,
};

type Point = {
  x: number;
  y: number;
};

const monoidPoint: Monoid<Point> = getStructMonoid({
  x: monoidSum,
  y: monoidSum,
});

type Vector = {
  from: Point;
  to: Point;
};

const monoidVector: Monoid<Vector> = getStructMonoid({
  from: monoidPoint,
  to: monoidPoint,
});

// const M = getApplyMonoid(monoidSum);
const M = getFirstMonoid<number>();

export const main = () => {
  console.log(fold(monoidSum)([1, 2, 3, 4]));
  console.log(M.concat(some(1), some(2)));
};
