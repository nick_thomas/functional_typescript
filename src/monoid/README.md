# Monoid

**What are the laws for Monoid?**

_Right identity_ : concat(x,empty) = x, for all x in A
_Left identity_ : concat(empty,x) = x, for all x in A

Whichever side of concat we put the value empty, it must make no difference to the value.

---

**What can getLastMonoid be useful for?**

Navigating optional values.
