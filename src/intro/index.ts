import {Either, tryCatch} from "fp-ts/lib/Either";
import {IO} from "fp-ts/lib/IO";
import {IOEither, tryCatch as tryCatchE} from "fp-ts/lib/IOEither";
import {fromNullable, none, Option, some} from "fp-ts/lib/Option";
import {Task} from "fp-ts/lib/Task";
import {readFileSync} from "fs";

function findIndex<A>(
	as: Array<A>,
	predicate: (a : A)=> boolean
) : Option<number>{
	const index = as.findIndex(predicate)
	return index === -1 ? none: some(index)
}


function find<A>(as: Array<A>,predicate: (a: A)=> boolean) : Option<A> {
	return fromNullable(as.find(predicate))
}


// Exceptions

function parse(s:string): Either<Error,unknown> {
	return tryCatch(
		()=> JSON.parse(s),
		(reason) => new Error(String(reason))
	)
}

//  Random Values
const random: IO<number> = () => Math.random()

// Synchronous side effects
//
function readFileSyncSafe(path:string) : IOEither<Error,string>{
	return tryCatchE(
		()=> readFileSync(path,'utf8'),
		(reason) => new Error(String(reason))
	)
}

// Async Side Effects
// https://dev.to/gcanti/interoperability-with-non-functional-code-using-fp-ts-432e
console.log(random())
