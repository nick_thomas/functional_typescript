** What is a type constructor?**

It is an n-ary type operator taking as argument zero or more types and returning another type.
